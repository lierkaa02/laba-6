CC = clang
COPTS = -g -O0 -lm

clean:
	rm -rf dist
prep:
	mkdir dist
compile: main.bin

main.bin: src/main.c
	$(CC) $(COPTS) $< -o ./dist/$@
run: clean prep compile
	./dist/main.bin
